## Downloading cosmic database using their help menu https://cancer.sanger.ac.uk/cosmic/help/file_download

## generated a base64 encoded HTTP authentication string
##echo 'loginusrname:password | base64'

## making authenticated request to get the download URL
curl -H "Authorization: Basic emFyaXR6YS5wZXRyb3ZhQHlhbGUuZWR1OkVHRlJFcmJCMTEkCg==" \https://cancer.sanger.ac.uk/cosmic/file_download/GRCh38/cosmic/v92/CosmicMutantExport.tsv.gz


## The request returns a url, which will be different each time. Input URL starting with https: and ending before the " into the single quote marks in the following line
## downloading the data file and saving as cosmicmutations.tsv.gz file   
curl -o cosmicmutations.tsv.gz ''


## unzip the .gz file with. Will generate a tab separated 16GB file with all gene mutations. File is called cosmicmutations.tsv
gunzip cosmicmutations.tsv.gz
