#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 20:58:22 2020

@author: Zaritza
"""

# Import packages used 
import os
import pandas as pd
    
## Choosing only columns of interest for each file of gene and writing to a new csv file
def csvwrite ():
    
    egfr_df = pd.read_csv(file)
    #print(egfr_df.head(5))

    genename = egfr_df.iloc[0, 0]




    egfrColDel_df = egfr_df.drop([' ACCESSION_NUMBER',' GENE_CDS_LENGTH', ' HGNC_ID', ' SAMPLE_NAME', ' ID_SAMPLE', ' ID_TUMOUR', ' SITE_SUBTYPE_1', ' SITE_SUBTYPE_2', ' SITE_SUBTYPE_3', ' PRIMARY_HISTOLOGY', ' HISTOLOGY_SUBTYPE_2', ' HISTOLOGY_SUBTYPE_3', ' GENOMIC_MUTATION_ID', ' LEGACY_MUTATION_ID',' MUTATION_ID', ' MUTATION_ZYGOSITY', ' LOH', ' GRCH', ' MUTATION_GENOME_POSITION', ' MUTATION_STRAND', ' SNP', ' HGVSP', ' HGVSC', ' HGVSG'],axis=1)


    resultname = (genename + '_genename.csv')
    egfrColDel_df.to_csv(resultname ,index=True)
    
    print (resultname)


## Main loop to iterate over files in cosmic downloaded files for each gene and perform choose column function

for file in os.listdir():
    if "V92" in file:
         csvwrite()

        
        
        



