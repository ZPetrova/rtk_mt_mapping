#!/bin/bash
#SBATCH --mail-user=zaritza.petrova@yale.edu
#SBATCH --mail-type=begin
#SBATCH	--mail-type=end
#SBATCH --output=csv_reorder.%J.out
#SBATCH --mem=5g
#SBATCH -t 90:00 # 1h 30 minutes


## Uncomment if wanting to schedule to run the singularity container as SLURM job 
#bash singularitycontainer.sh


bash rsynctoOutput.sh

