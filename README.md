# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for code and files to extract coding point mutations from targeted and genome wide screens from the COSMIC database or work with already downloaded .csv files for a
  particular gene from the database. The CSV files contain 40columns, but are reorganized so that only the most relevant columns are kept.  
  

* Version 2.1.0  

### How do I get set up? ###
#### Downloading a CSV file mutations for a coding point mutations of gene manually from COSMIC.
* Must set-up a login account on <https://cancer.sanger.ac.uk/cosmic>
* Go to <https://cancer.sanger.ac.uk/cosmic/download>. 
* Scroll down to COSMIC Mutation Data.
* Click download filtered file tab, input gene of interest ex:EGFR and download .csv file to Mutation_files_grace/ folder

#### Downloading entire database of coding point mutations for all genes:
* follow the prompts of the APIcalltoCosmic.sh file. Must have a COSMIC account. Then run script with `bash APIcalltoCosmic.sh`
* example script to extract first100rows from `cosmicmutations.tsv` is the `cosmicgenes_first100rows.py` script. Note: to run in singularity container, must start with V92_ in filename.

  
#### How to run script to reorganize .csv files, either manually downloaded or a subset extracted from entire database download
* From *Mutation_files_grace/* folder, run `bash singularitycontainer.sh`
* To move new CSV files to *output_Mutation_files_reordered/* folder, `bash rsynctoOutput.sh` 
* Edit batch.sh SLURM file to schedule only `bash singularitycontainer.sh` or `bash rsynctoOutput.sh` or both.

#### 2020-Summer-PyMOL Submodule: Mapping ready srcipts on Pymol
* Dependencies: Pymol running Python3 
* CSV files and python script for gene must be in same directory and this must be the Pymol working directory
* in Pymol run `run script.py`


### Versioning ###
* 2.1.0 Pymol script  for extraction rows from cosmicmutations file
* 2.0.0 Pymol scripts for mutation mappings
* 1.0.0 Public Release









  



  




