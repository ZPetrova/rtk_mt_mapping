FROM python:3
RUN python -m pip install pandas 
RUN python -m pip install numpy


RUN mkdir /opt/genes

ADD . /opt/genes

WORKDIR /opt/genes/Python_scripts

RUN chmod 777 /opt/genes/Python_scripts/CSVofGene.py

CMD ["python","CSVofGene.py"]
